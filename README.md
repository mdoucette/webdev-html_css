Web Development with HTML/CSS and JavaScript
============================================

This project covers subject matter from web technologies.
It is a static site using HTML/CSS and perhaps a little JavaScript thrown in for good measure.

I will continue to use it for as long as necessary as the base code for my practice and ideas while learning these
technologies.

It is free to use if you find some code or techniques here that interest you.