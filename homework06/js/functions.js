/**
 * Created by mdoucette on 2014-03-17.
 * Functions for homework 06
 */

function changeColor() {
    document.bgColor = "darkolivegreen";
    document.fgColor = "#ffffff";
}
function changeColorBack() {
    document.bgColor = "#ffffff";
    document.fgColor = "#000000";
}
/*
 note: this function requires a parameter
 the invoking instruction must provide additional data,
 eg:	changeCustomBackgroundColor('#ababab');
 */
function changeCustomBackgroundColor(color) {
    document.bgColor = color;
    document.fgColor = "#000000";
}